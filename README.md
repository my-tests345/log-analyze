# log-analyze

## Подготовка проекта к запуску

make docker-build

## Запуск проекта

make docker-up

## Запуск тестов

make test
