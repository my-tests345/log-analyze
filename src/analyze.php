<?php

define('PROJECT_START', microtime(true));
require __DIR__ . '/vendor/autoload.php';

$analyze = new App\LogAnalyze();
$logOutput = new App\LogOutput();

while ($inputStreamString = fgets(STDIN)) {
    try {
        if ($rejectedLog = $analyze->execute($inputStreamString)) {
            $rejectedLogData = $logOutput->prepare($rejectedLog, [3, 10]);
            $logTime = date('H:i:s', strtotime($rejectedLogData[3]));
            $rejectedLogDataInterval = $logOutput->getInterval(new DateTime($logTime), $rejectedLogData[10]);
            $rejectedLogOutput = $logOutput->output($rejectedLogDataInterval);
            var_dump($rejectedLogOutput);
        }
    } catch (Exception $e) {
    }
}
