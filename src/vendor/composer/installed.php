<?php return array(
    'root' => array(
        'name' => 'ilya/check-logs',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'behat/gherkin' => array(
            'pretty_version' => 'v4.9.0',
            'version' => '4.9.0.0',
            'reference' => '0bc8d1e30e96183e4f36db9dc79caead300beff4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../behat/gherkin',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'codeception/codeception' => array(
            'pretty_version' => '2.5.6',
            'version' => '2.5.6.0',
            'reference' => 'b83a9338296e706fab2ceb49de8a352fbca3dc98',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/codeception',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'codeception/mockery-module' => array(
            'pretty_version' => '0.2.3',
            'version' => '0.2.3.0',
            'reference' => 'd01f9d0c52cf4fa87c789c65191027a0d434bd3d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/mockery-module',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'codeception/phpunit-wrapper' => array(
            'pretty_version' => '7.8.4',
            'version' => '7.8.4.0',
            'reference' => 'dd44fc152433d27d3de03d59b4945449b3407af0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/phpunit-wrapper',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'codeception/stub' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'reference' => '853657f988942f7afb69becf3fd0059f192c705a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/stub',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'cordoval/hamcrest-php' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'davedevelopment/hamcrest-php' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => 'v1.1.1',
            'version' => '1.1.1.0',
            'reference' => '612a3ee5ab0d5dd97b7cf3874a6efe24325efac3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'doctrine/instantiator' => array(
            'pretty_version' => '1.5.0',
            'version' => '1.5.0.0',
            'reference' => '0a0fa9780f5d4e507415a065172d26a98d02047b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'facebook/webdriver' => array(
            'pretty_version' => '1.7.1',
            'version' => '1.7.1.0',
            'reference' => 'e43de70f3c7166169d0f14a374505392734160e5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../facebook/webdriver',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '6.5.8',
            'version' => '6.5.8.0',
            'reference' => 'a52f0440530b54fa079ce76e8c5d196a42cad981',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.3',
            'version' => '1.5.3.0',
            'reference' => '67ab6e18aaa14d753cc148911d273f6e6cb6721e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '1.9.1',
            'version' => '1.9.1.0',
            'reference' => 'e4490cabc77465aaee90b20cfc9a770f8c04be6b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'hamcrest/hamcrest-php' => array(
            'pretty_version' => 'v2.0.1',
            'version' => '2.0.1.0',
            'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../hamcrest/hamcrest-php',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'ilya/check-logs' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'kodova/hamcrest-php' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'mockery/mockery' => array(
            'pretty_version' => '1.3.6',
            'version' => '1.3.6.0',
            'reference' => 'dc206df4fa314a50bbb81cf72239a305c5bbd5c0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mockery/mockery',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'myclabs/deep-copy' => array(
            'pretty_version' => '1.11.1',
            'version' => '1.11.1.0',
            'reference' => '7284c22080590fb39f2ffa3e9057f10a4ddd0e0c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/deep-copy',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phar-io/manifest' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/manifest',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phar-io/version' => array(
            'pretty_version' => '2.0.1',
            'version' => '2.0.1.0',
            'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/version',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpdocumentor/reflection-common' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpdocumentor/reflection-common',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpdocumentor/reflection-docblock' => array(
            'pretty_version' => '5.3.0',
            'version' => '5.3.0.0',
            'reference' => '622548b623e81ca6d78b721c5e029f4ce664f170',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpdocumentor/reflection-docblock',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpdocumentor/type-resolver' => array(
            'pretty_version' => '1.7.3',
            'version' => '1.7.3.0',
            'reference' => '3219c6ee25c9ea71e3d9bbaf39c67c9ebd499419',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpdocumentor/type-resolver',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpspec/prophecy' => array(
            'pretty_version' => 'v1.17.0',
            'version' => '1.17.0.0',
            'reference' => '15873c65b207b07765dbc3c95d20fdf4a320cbe2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpspec/prophecy',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpstan/phpdoc-parser' => array(
            'pretty_version' => '1.23.1',
            'version' => '1.23.1.0',
            'reference' => '846ae76eef31c6d7790fac9bc399ecee45160b26',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpstan/phpdoc-parser',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-code-coverage' => array(
            'pretty_version' => '6.1.4',
            'version' => '6.1.4.0',
            'reference' => '807e6013b00af69b6c5d9ceb4282d0393dbb9d8d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-code-coverage',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-file-iterator' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'reference' => '42c5ba5220e6904cbfe8b1a1bda7c0cfdc8c12f5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-file-iterator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-text-template' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-text-template',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-timer' => array(
            'pretty_version' => '2.1.3',
            'version' => '2.1.3.0',
            'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-timer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-token-stream' => array(
            'pretty_version' => '3.1.3',
            'version' => '3.1.3.0',
            'reference' => '9c1da83261628cb24b6a6df371b6e312b3954768',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-token-stream',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/phpunit' => array(
            'pretty_version' => '7.5.20',
            'version' => '7.5.20.0',
            'reference' => '9467db479d1b0487c99733bb1e7944d32deded2c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/phpunit',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'psr/event-dispatcher-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.1',
            'version' => '1.1.0.0',
            'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/code-unit-reverse-lookup' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/code-unit-reverse-lookup',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/comparator' => array(
            'pretty_version' => '3.0.5',
            'version' => '3.0.5.0',
            'reference' => '1dc7ceb4a24aede938c7af2a9ed1de09609ca770',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/comparator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/diff' => array(
            'pretty_version' => '3.0.4',
            'version' => '3.0.4.0',
            'reference' => '6296a0c086dd0117c1b78b059374d7fcbe7545ae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/diff',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/environment' => array(
            'pretty_version' => '4.2.4',
            'version' => '4.2.4.0',
            'reference' => 'd47bbbad83711771f167c72d4e3f25f7fcc1f8b0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/environment',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/exporter' => array(
            'pretty_version' => '3.1.5',
            'version' => '3.1.5.0',
            'reference' => '73a9676f2833b9a7c36968f9d882589cd75511e6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/exporter',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/global-state' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => 'e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/global-state',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/object-enumerator' => array(
            'pretty_version' => '3.0.4',
            'version' => '3.0.4.0',
            'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-enumerator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/object-reflector' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-reflector',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/recursion-context' => array(
            'pretty_version' => '3.0.1',
            'version' => '3.0.1.0',
            'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/recursion-context',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/resource-operations' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/resource-operations',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/version' => array(
            'pretty_version' => '2.0.1',
            'version' => '2.0.1.0',
            'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/version',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/browser-kit' => array(
            'pretty_version' => 'v4.4.44',
            'version' => '4.4.44.0',
            'reference' => '2a1ff40723ef6b29c8229a860a9c8f815ad7dbbb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/browser-kit',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/console' => array(
            'pretty_version' => 'v4.4.49',
            'version' => '4.4.49.0',
            'reference' => '33fa45ffc81fdcc1ca368d4946da859c8cdb58d9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/css-selector' => array(
            'pretty_version' => 'v4.4.44',
            'version' => '4.4.44.0',
            'reference' => 'bd0a6737e48de45b4b0b7b6fc98c78404ddceaed',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/css-selector',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/dom-crawler' => array(
            'pretty_version' => 'v4.4.45',
            'version' => '4.4.45.0',
            'reference' => '4b8daf6c56801e6d664224261cb100b73edc78a5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/dom-crawler',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v4.4.44',
            'version' => '4.4.44.0',
            'reference' => '1e866e9e5c1b22168e0ce5f0b467f19bba61266a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/event-dispatcher-contracts' => array(
            'pretty_version' => 'v1.1.13',
            'version' => '1.1.13.0',
            'reference' => '1d5cd762abaa6b2a4169d3e77610193a7157129e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher-contracts',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/event-dispatcher-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.1',
            ),
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v4.4.44',
            'version' => '4.4.44.0',
            'reference' => '66bd787edb5e42ff59d3523f623895af05043e4f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '639084e360537a19f9ee352433b84ce831f3d2da',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '869329b1e9894268a8a61dabb69153029b7a8c97',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '9e8ecb5f92152187c4799efd3c96b78ccab18ff9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/process' => array(
            'pretty_version' => 'v4.4.44',
            'version' => '4.4.44.0',
            'reference' => '5cee9cdc4f7805e2699d9fd66991a0e6df8252a2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v4.4.45',
            'version' => '4.4.45.0',
            'reference' => 'aeccc4dc52a9e634f1d1eebeb21eacfdcff1053d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'theseer/tokenizer' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../theseer/tokenizer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'webmozart/assert' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webmozart/assert',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
    ),
);
