<?php

namespace App;

use App\Abstracts\AbstractAnalyze;
use App\Rules\RuleFactory;
use Exception;

class LogAnalyze extends AbstractAnalyze
{
    /**
     * @throws Exception
     */
    public function execute(string $value): string
    {
        $data = $this->prepare($value);

        return $this->isRejected($data) ? $value : '';
    }

    /**
     * @throws Exception
     */
    private function isRejected(array $log): bool
    {
        $ruleFactory = new RuleFactory();

        foreach ($log as $key => $value) {
            $rule = $ruleFactory::getRule($key, $value);

            if (!empty($rule)) {
                $allowValue = null;
                if (!empty($rule->allowValueIndex)) {
                    $allowValue = $this->getArgValue($rule->allowValueIndex);
                }

                if (!$rule->rejected($value, $allowValue)) {
                    return true;
                }
            }
        }

        return false;
    }

    private function getArgValue(string $index)
    {
        $value = null;

        if($params = $_SERVER["argv"]) {
            array_shift($params);
            while($params) {
                $param = array_shift($params);
                $paramArg = str_replace('-', '', $param);
                if ($paramArg === $index) {
                    $value = array_shift( $params);
                }
            }
        }
        return $value;
    }
}
