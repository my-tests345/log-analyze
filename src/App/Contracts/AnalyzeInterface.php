<?php

namespace App\Contracts;

interface AnalyzeInterface
{
    public function prepare(string $value): array;

    public function execute(string $value): string;
}
