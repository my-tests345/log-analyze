<?php

namespace App\Contracts;

interface RuleInterface
{
    public function rejected(string $rule, array $allows): bool;
}
