<?php

namespace App;

use DateInterval;
use DateTime;
use Exception;

class LogOutput
{
    public function prepare(string $log, array $items): array
    {
        $logData = explode(' ', str_replace(['[', ']', '"'], '', $log));

        return array_intersect_key(array_values($logData), array_flip($items));
    }

    public function output(array $data): string
    {
        return implode(' ', $data);
    }

    /**
     * @throws Exception
     */
    public function getInterval(DateTime $date, string $diffTime): array
    {
        $startTime = date('H:i:s', $date->getTimestamp());
        $finishTime = date('H:i:s', $date->add(new DateInterval('PT' . round($diffTime) . 'S'))->getTimestamp());

        return [$startTime, $finishTime];
    }
}