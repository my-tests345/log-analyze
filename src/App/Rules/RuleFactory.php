<?php

namespace App\Rules;

class RuleFactory
{
    protected const RULE_KEYS = [
        'HttpCode' => 8,
        'ResponseTime' => 10,
    ];

    public static function getRule(int $ruleKey, $value, $allowValue = null)
    {
        $rule = 'App\\Rules\\' . ucfirst(self::getRuleNameByKey($ruleKey)) . 'Rule';

        if (class_exists($rule)) {
            return new $rule($value, $allowValue);
        }

        return null;
    }

    private static function getRuleNameByKey(int $ruleKey)
    {
        return array_search($ruleKey, self::RULE_KEYS);
    }
}
