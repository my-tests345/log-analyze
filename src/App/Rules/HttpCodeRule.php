<?php

namespace App\Rules;

use App\Abstracts\AbstractRule;

class HttpCodeRule extends AbstractRule
{
    private array $allows = [
        200,
        204,
    ];

    public function rejected($value, $allowValue = null): bool
    {
        if (!empty($allowValue)) {
            $this->allows[] = $allowValue;
        }

        return parent::isRejected($value, $this->allows);
    }
}
