<?php

namespace App\Rules;

use App\Abstracts\AbstractRule;

class ResponseTimeRule extends AbstractRule
{
    private array $allows = [];

    public string $allowValueIndex = 't';

    public function rejected($value, $allowValue = null): bool
    {
        if (!empty($allowValue)) {
            $this->allows[] = (int)$allowValue;
        }

        return parent::isRejected($value, $this->allows, '>');
    }
}
