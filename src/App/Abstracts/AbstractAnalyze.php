<?php

namespace App\Abstracts;

use App\Contracts\AnalyzeInterface;

abstract class AbstractAnalyze implements AnalyzeInterface
{
    public function prepare(string $value): array
    {
        return explode(' ', $value);
    }

    abstract public function execute(string $value): string;
}
