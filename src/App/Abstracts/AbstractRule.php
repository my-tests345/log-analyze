<?php

namespace App\Abstracts;

use App\Contracts\RuleInterface;

abstract class AbstractRule implements RuleInterface
{
    abstract public function rejected(string $rule, array $allows): bool;

    public static function isRejected(string $value, array $allows, string $condition = '='): bool
    {
        if (empty($allows)) {
            return true;
        }

        if ($condition !== '=' && mb_strlen($condition) === 1) {
            foreach ($allows as $allow) {
                switch ($condition) {
                    case ('>'):
                        if ($allow > $value) {
                            return true;
                        }
                        break;
                    case ('<'):
                        if ($allow < $value) {
                            return true;
                        }
                        break;
                }
//                if (eval("$allow $condition $value;")) {
//                    return true;
//                }
            }
        }

        return in_array($value, $allows);
    }
}
